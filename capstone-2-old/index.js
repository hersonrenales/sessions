// Dependencies and Modules___________________________________
	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");
	require('dotenv').config();
	const userRoutes = require("./routes/user.js"); 
	const productRoutes = require("./routes/product.js"); 
// Environment Setup
	const port = 4000;
// Server Setup_______________________________________________
	const app = express();
	app.use(express.json());
	app.use(express.urlencoded({extended:true}));
	app.use(cors()); 
// Database Connection________________________________________
	mongoose.connect("mongodb+srv://admin:admin123@cluster0.4skkann.mongodb.net/Capstone-2?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
	mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas!"));
// Backend Routes____________________________________________
	app.use("/users", userRoutes);
	app.use("/products", productRoutes);

// Server Gateway Response___________________________________
	if(require.main === module){
		app.listen(process.env.PORT || port, () => {
			console.log(`API is now online on port ${process.env.PORT || port}`)
		})
	}
// ___________________________________________________________
module.exports = app;
