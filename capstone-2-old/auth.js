const jwt = require("jsonwebtoken");
const secret = "ProductBookingAPI";
// Token Creation_______________________________________
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		userName: user.userName,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
};
// Verify Token_________________________________________
module.exports.verify = (req,res,next) => {
	console.log(req.headers.authorization);
	let token = req.headers.authorization;
	if(typeof token === "undefined"){
		return res.send({auth: "Failed! No Token"})
	}else{
		console.log(token)
		token = token.slice(7, token.length);
		console.log(token);
		jwt.verify(token,secret, function(error, decodedToken){
			if(error){
				return res.send({
					auth: "Failed",
					message: error.message
				});
			}else{
				req.user = decodedToken;
				next();
			}
		})
	}
};
// Verify Admin Access__________________________________
module.exports.verifyAdmin = (req,res,next) => {
	if(req.user.isAdmin){
		next()
	}else{
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}