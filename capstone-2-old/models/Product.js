const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	stock: {
		type: Number,
		required: [true, "Stock is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [
		{
			/*userId: {
				type: String,
				required: [true, "User ID is required"]
			},*/
			userName: {
				type: String,
				required: [true, "Username is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			}
		}

	]
});

module.exports = mongoose.model("Product", productSchema);