const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	userName: {
		type: String,
		required: [true, "Username is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProduct: [
		{
			product: [
				{
					/*productId: {
						type: String,
						required: [true, "Product ID is required"]
					},*/
					productName: {
						type: String,
						required: [true, "Product name is required"]
					},
					quantity: {
						type: Number,
						required: [true, "Quantity is required"]
					}
				}

			],
			userName: {
				type: String,
				required: [true, "Username is required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}

		}

	]
});

module.exports = mongoose.model("User", userSchema);