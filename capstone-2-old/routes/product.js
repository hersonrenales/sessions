const express = require("express");
const productController = require("../controllers/product.js");
const auth = require("../auth.js")
// Destructure from auth
const {verify, verifyAdmin} = auth;
// Routing Component
const router = express.Router();

// Create Product (Admin Only)
router.post("/", verify, verifyAdmin, productController.addProduct);

// Get All Product
router.get("/allproducts", productController.getAllProducts);

// Get All "Active" Product
router.get("/activeproducts", productController.activeProducts);

// Get Specific Product
router.get("/1/:productName", productController.getProductByName);

// Get Specific Product
router.get("/2/:productId", productController.getProductById);

// Update Product Details by Name(Admin Only)
router.put("/1/:productName", verify, verifyAdmin, productController.updateProductByName);

// Update Product Details by Name(Admin Only)
router.put("/2/:productId", verify, verifyAdmin, productController.updateProductById);

// Deactivate a Product by Name
router.put("/1/:productName/deactivate", verify, verifyAdmin, productController.deactivateProductByName);
// Deactivate a Product by ID
router.put("/2/:productId/deactivate", verify, verifyAdmin, productController.deactivateProductById);

// Activate a Product by Name
router.put("/1/:productName/activate", verify, verifyAdmin, productController.activateProductByName);
// Activate a Product by ID
router.put("/2/:productId/activate", verify, verifyAdmin, productController.activateProductById);

// Export Route System
module.exports = router;