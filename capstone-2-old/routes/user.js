const express = require("express");
const userController = require("../controllers/user.js");
const auth = require("../auth.js")
// Destructure from auth
const {verify, verifyAdmin} = auth;
// Routing Component
const router = express.Router();

// User Registration Routes
router.post("/registration", (req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Login (Authentication)
router.post("/login", userController.loginUser)

// Check User Exists by Name or Email
router.get("/checkuserexist", userController.checkUser)

// Check User Details
router.get("/checkuserdetails", verify, userController.checkUserDetails);

// Buy a Product (Create Order)
router.post("/order", verify, userController.order);

// Cancel a Ordered Product (Cancel Order)
router.put("/cancelorder", verify, userController.cancelOrder);

// Set User as Admin (Admin Only)
router.put("/setasadmin", verify, verifyAdmin, userController.setAsAdmin);

// Remove User as Admin (Admin Only)
router.put("/removeasadmin", verify, verifyAdmin, userController.removeAsAdmin);

// Export Route System
module.exports = router;