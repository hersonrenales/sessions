const Product = require("../models/Product.js");
const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Add Product (Admin Only)
module.exports.addProduct = async (req,res) => {
	console.log(req.body);
	const existingProduct = await Product.findOne({productName: req.body.productName});
  	if(existingProduct){
  		return res.send("Same product is already registered.")
  	}else {
		let newProduct = new Product({
			productName: req.body.productName,
			description: req.body.description,
			stock: req.body.stock,
			price: req.body.price
		});
		return newProduct.save().then((product,error) => {
			if(error){
				return res.send("Sorry! Failed to add new product.")
			}else{
				return res.send("New product successfully added.")
			}
		})
		.catch(error => res.send(error));
	}
};

// Get All Products
module.exports.getAllProducts = (req,res) => {
	return Product.find({}).then(result =>{
		if(result.length === 0){
			return res.send("There is no product available.")
		}else{
			return res.send(result);
		}
	})
}

// Get All Active Prodcuts
module.exports.activeProducts = (req,res) => {
	return Product.find({isActive: true}).then(result => {
		if(result.length === 0){
			return res.send("There is no active product available.")
		}else{
			return res.send(result);
		}
	})
}

// Get Specific Product by Name
module.exports.getProductByName = (req,res) => {
	return Product.findOne({productName: req.params.productName}).then(result => {
		if(result === 0){
			return res.send("Product does not exist.")
		}else{
			if(result.isActive === false){
				return res.send("The product exists but currently inactive.");
			}else{
				return res.send(result);
			}
		}
	})
	.catch(error => res.send("Please enter a correct product name"))
}

// Get Specific Product by Id
module.exports.getProductById = (req,res) => {
	return Product.findById(req.params.productId).then(result => {
		if(result === 0){
			return res.send("Product does not exist.")
		}else{
			if(result.isActive === false){
				return res.send("The product exists but currently inactive.");
			}else{
				return res.send(result);
			}
		}
	})
	.catch(error => res.send("Please enter a correct product ID"))
}

// Update Product Details by Name
module.exports.updateProductByName=(req,res) => {
	let updatedProduct = {
		productName: req.body.name,
		description: req.body.description,
		stock: req.body.stock,
		price:req.body.price
	}
	return Product.findOneAndUpdate({productName: req.params.productName}, updatedProduct).then((product,error) => {
		if(error){
			return res.send("Sorry! Failed to update the product.");
		}else{
			return res.send(`${req.params.productName} is now updated.`);
		}
	})
}

// Update Product Details by ID
module.exports.updateProductById=(req,res) => {
	let updatedProduct = {
		productName: req.body.name,
		description: req.body.description,
		stock: req.body.stock,
		price:req.body.price
	}
	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product,error) => {
		if(error){
			return res.send("Sorry! Failed to update the product.");
		}else{
			return res.send(`${product.productName} is now updated.`);
		}
	})
}

// Deactivating a Product by Name
module.exports.deactivateProductByName = (req, res) => {
  const productName = req.params.productName;

  Product.findOne({productName}).then((product) => {
      if (product === null) {
        return res.send("Product name does not exist.");
      }else if (product.isActive === false) {
        return res.send("Product is already inactive.");
      }else{
        const deactivateField = {isActive: false};
        Product.findOneAndUpdate({productName}, deactivateField)
          .then((updatedProduct, error) => {
            if (error) {
              return res.send("Sorry! Failed to activate the product.");
            } else {
              return res.send("Product is successfully deactivated.");
            }
          })
      }
    })
    .catch(error => res.send(error));
};

// Deactivating a Product by ID
module.exports.deactivateProductById = (req, res) => {
  Product.findById(req.params.productId).then((product) => {
    if (product.isActive === false) {
      return res.send("Product is already inactive.");
    } else {
      const deactivateField = { isActive: false };
      Product.findByIdAndUpdate(req.params.productId, deactivateField)
        .then((updatedProduct, error) => {
          if (error) {
            return res.send("Sorry! Failed to deactivate the product.");
          } else {
            return res.send("Product is successfully deactivated.");
          }
        });
    }
  })
  .catch(error => res.send("Product ID does not exist."));
};

// Activating a Product by Name
module.exports.activateProductByName = (req, res) => {
  const productName = req.params.productName;

  Product.findOne({productName}).then((product) => {
      if (product === null) {
        return res.send("Product name does not exist.");
      }else if (product.isActive === true) {
        return res.send("Product is already active.");
      }else{
        const activateField = {isActive: true};
        Product.findOneAndUpdate({productName}, activateField)
          .then((updatedProduct, error) => {
            if (error) {
              return res.send("Sorry! Failed to activate the product.");
            } else {
              return res.send("Product is successfully activated.");
            }
          })
      }
    })
    .catch(error => res.send(error));
};

// Activating a Product by ID
module.exports.activateProductById = (req, res) => {
  Product.findById(req.params.productId).then((product) => {
    if (product.isActive === true) {
      return res.send("Product is already active.");
    } else {
      const activateField = { isActive: true };
      Product.findByIdAndUpdate(req.params.productId, activateField)
        .then((updatedProduct, error) => {
          if (error) {
            return res.send("Sorry! Failed to deactivate the product.");
          } else {
            return res.send("Product is successfully deactivated.");
          }
        });
    }
  })
  .catch(error => res.send("Product ID does not exist."));
};