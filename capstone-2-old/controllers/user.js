const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// User Registration
module.exports.registerUser = async (reqBody) => {
	console.log(reqBody);
  	const existingUser = await User.findOne({ $or: [{ email: reqBody.email }, { userName: reqBody.userName }] });

  	if(existingUser){
  		return ("Username or Email is already registered.")
  	}else {
		let newUser = new User({
			userName: reqBody.userName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password,10)
		});
		return newUser.save().then((user,error) => {
			if(error){
				return ("Sorry! Failed to register new user.")
			}else{
				return ("You have successfully registered " + newUser.userName)
			}
		})
		.catch(err => err);
	}
};

// User Login (Authentication)
module.exports.loginUser = (req,res) => {
	return User.findOne({ $or: [{email: req.body.email }, { userName: req.body.userName }] }).then(result => {
		console.log(result);
		if(result == null){
			return res.send("Check Email or Username.");
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			console.log("Is the password correct? " + isPasswordCorrect);
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}else{
				return res.send("Password is incorrect.");
			}
		}
	})
	.catch(err => res.send(err));
}

// Check User Exists by Name or Email
module.exports.checkUser = (req,res) => {
	return User.find({$or:[{email: req.body.email}, {userName: req.body.userName}]}).then(result =>{
		console.log(result);
		if(result.length > 0){
			return res.send("User exists.")
		}else{
			return res.send("User not found. Please check username/email.")
		};
	});
};

// Check User Details
module.exports.checkUserDetails = (req,res) => {
	return User.findOne({
		$or:[
				{email: req.body.email}, 
				{userName: req.body.userName},
				{_id: req.body._id},
			]}).then(result => {
		result.password = "Password Encrypted"
		return res.send(result);
	})
	.catch(error => res.send("User does not exist. Please check username/email"));
}

// Buy a Product (Create Order)
module.exports.order = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send("Admin is not allowed to buy a product.");
  }
  try {
    // Check product stock
    Product.findOne({ productName: req.body.productName }).then((product) => {
      if (product.stock < req.body.quantity) {
        return res.send("Insufficient stock");
      }

      // User
      User.findOne({ userName: req.user.userName }).then(async (user) => {
        if (user === null) {
          return res.send({ message: "User not found" });
        }
        let order = {
          productName: req.body.productName,
          quantity: req.body.quantity,
        };
        let orderedProduct = {
          product: [order],
          userName: req.user.userName,
          totalAmount: req.body.totalAmount,
        };
        user.orderedProduct.push(orderedProduct);
        await user.save();

        // Update product stock
        const newStock = { stock: product.stock - req.body.quantity };
        Product.findOneAndUpdate(
          { productName: req.body.productName },
          newStock
        ).then((updatedProduct, error) => {
          if (error) {
            return res.send("Sorry! Failed to deduct order from stock.");
          }

          // Product
          let buyer = {
            userName: req.user.userName,
            totalAmount: req.body.totalAmount,
            quantity: req.body.quantity,
          };
          product.userOrders.push(buyer);
          product.save(); // No need to use await here
          return res.send("You have successfully ordered this product.");
        });
      });
    });
  } catch (error) {
    return res.send("Sorry! Failed to order product");
  }
};

// Cancel an Ordered Product
module.exports.cancelOrder = async (req, res) => {
	if (req.user.isAdmin) {
    return res.send("Admin is not allowed to buy a product.");
  }
  try {
  	// User
  	 let user = await User.findOne({userName: req.user.userName});
    if (user === null) {
      return res.send({ message: "User not found" });
    }
    user.orderedProduct = user.orderedProduct.filter((order) => {
      return (
        order.product.productName !== req.body.productName &&
        order.product.quantity !== req.body.quantity
      );
    });
    await user.save();

    // Product
    let product = await Product.findOne({productName: req.body.productName});
    if (product === null) {
      return res.send({ message: "Product not found" });
    }
    // Revert Stock Quantity
    const revertQuantity = product.userOrders[0].quantity
		const revertStock = product.stock + revertQuantity
    await Product.findOneAndUpdate(
      { productName: req.body.productName},
      {stock: revertStock}
    );

    // Product Save
    product.userOrders = product.userOrders.filter(
      (buyer) => buyer.userName !== req.user.userName
    );
    await product.save();
    return res.send("You have successfully cancelled ordered this product.");

  }catch (error) {return res.send("Sorry! Failed to cancel ordered product");
  }
}

// Set User as Admin (Admin Only)
module.exports.setAsAdmin = (req,res) => {
	const userName = req.body.userName;
	User.findOne({userName}).then((user) => {
		if(user === null) {
			return res.send("User does not exists.")
		}else if(user.isAdmin === true){
			return res.send("User is already an admin.")
		}else{
			const setAdmin = {isAdmin: true}
			User.findOneAndUpdate({userName}, setAdmin)
        .then((updatedUser, error) => {
          if (error) {
            return res.send("Sorry! Failed to set user as admin.");
          } else {
            return res.send("User is successfully set as admin.");
          }
        });
		};
	}).catch(err => res.send(err));
};


// Remove User as Admin (Admin Only)
module.exports.removeAsAdmin = (req,res) => {
	const userName = req.body.userName;
	User.findOne({userName}).then((user) => {
		if(user === null) {
			return res.send("User does not exists.")
		}else if(user.isAdmin === false){
			return res.send("User is already not an admin.")
		}else{
			const removeAdmin = {isAdmin: false}
			User.findOneAndUpdate({userName}, removeAdmin)
        .then((updatedUser, error) => {
          if (error) {
            return res.send("Sorry! Failed to remove user as admin.");
          } else {
            return res.send("User is successfully removed as admin.");
          }
        });
		};
	}).catch(err => res.send(err));
};

