// [ SECTION ] Dependecies and Modules
const User = require('../models/User');
const Product = require("../models/Product.js");
const bcrypt = require('bcrypt');

const auth = require('../auth');

// check email exists
/*
	Steps:
		1. Use mongoose 'find' method to find duplicate emails
		2. Use the 'then' method to send a response back to the frontend application based on the result of the "find" method.
*/
module.exports.checkEmailExist = (reqBody) => {
	console.log(reqBody);
	console.log(reqBody.email);
	return User.find({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result.length > 0){
			return true
		} else {
			return false
		};
	});
};

// user registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
*/
module.exports.registerUser = (reqBody) => {
	console.log(reqBody);
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
		image: reqBody.image
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
	.catch(err => err);
};

// user authentication
//module.exports.loginUser = (req, res) => {
module.exports.loginUser = (reqBody) => {

	//return User.findOne({ email: req.body.email }).then(result => {
	return User.findOne({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result === null){
			//return res.send(false); // the email doesn't exist in our DB
			return false; // the email doesn't exist in our DB
		} else {

			//const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				//return res.send({ access: auth.createAccessToken(result)})
				return { access: auth.createAccessToken(result)}
			} else {
				return false; // Passwords do not match
			}
		}
	})
	// .catch(err => res.send(err))
}

// Retrieve user details

module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "";
		return res.send(result);
	})
	.catch(error => error)
}

module.exports.order = async (req, res) => {
	console.log(req.user.id);
	console.log(req.body.productId);

	if(req.user.isAdmin){
		return false;
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		let newOrder = {
			productId: req.body.productId,

		}

		let listOfOrders = user.orders;

		let filteredProductId = [];

		listOfOrders.forEach((result) => {
			filteredProductId.push(result.productId);
		});

		console.log(filteredProductId);

		let orderDuplicate = filteredProductId.includes(req.body.productId);

		console.log("order duplicate: " + orderDuplicate);

		if(orderDuplicate){
			return false;
		}

			user.orders.push(newOrder);


		return user.save().then(user => true).catch(error => res.send(error));

	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isProductUpdated = await Product.findById(req.body.productId).then(Product => {
		let order = {
			userId: req.user.id
		}

		product.orderedProduct.push(order);

		return product.save().then(product => true).catch(error => res.send(error));
	})

	if(isProductUpdated !== true){
		return res.send({message: isProductUpdated});
	}

	if(isUserUpdated && isProductUpdated){
		return res.send({message:true});
	}
}

module.exports.getOrders = (req, res) => {
	User.findById(req.user.id).then(result => {
		if(result.orders == [] || result.orders == null){
			return res.send("You have not ordered to any product.")
		}else{
			res.send(result.orders);
		}
	}).catch(error => res.send(error));
}


module.exports.resetPassword = async (req, res) => {
	try{
		const newPassword = req.body.newPassword;
		const userId = req.user.id;

		// Hashing the new password
		const hashedPassword = await bcrypt.hash(newPassword, 10);

		await User.findByIdAndUpdate(userId, {password: hashedPassword});

		res.status(200).json({message: "Password reset successfully!"});
	}catch(error){
		res.status(500).json({message: "Internal Server Error"});
	}
}

