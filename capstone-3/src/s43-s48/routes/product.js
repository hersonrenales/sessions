const express = require("express");
const productController = require("../controllers/product.js");

const auth = require("../auth.js");

// destructing of verify and verifyAdmin

const {verify, verifyAdmin} = auth;

const router = express.Router();

// Create a product POST
router.post("/", verify, verifyAdmin, productController.addProduct);

// Get all products
router.get("/all", productController.getAllProducts);

// Get all "active" product
router.get("/", productController.getAllActive);

// Get 1 specific product using its ID
router.get("/:productId", productController.getProduct);

// Updating a product (Admin Only)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// Archive a product
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// Activating a product
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

router.post("/archives", verify, verifyAdmin, productController.archives);

module.exports = router;