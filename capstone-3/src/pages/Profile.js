import {Row,Col,Card} from 'react-bootstrap';
import UserContext from '../UserContext';
import {useContext, useState, useEffect} from 'react'
import {Navigate} from 'react-router-dom';    
import UpdateProfileForm from '../components/UpdateProfile';
import Container from 'react-bootstrap/Container';

export default function Profile() {
	const {user} = useContext(UserContext);
    const [details,setDetails] = useState({});
    useEffect(()=>{

        fetch(`https://capstone2-renales.onrender.com/users/details`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {            
            if (typeof data._id !== "undefined") {
                setDetails(data);
            }
        });

    },[])
	return(
		(user.id === null)?
        <Navigate to="/products"/>
      :
      <>
        <Container>
          <Row className="profile px-5 text-center">
            <Col>
              <h1 className="mt-5 pt-5">Profile</h1>
              <Card.Img src={details.image} style={{ maxWidth: "200px", maxHeight: "200px" }} />
              <h2 className="mt-5">{details.firstName + " " + details.lastName}</h2>
              <hr />
              <h4>Contacts</h4>
              <ul className="px-5">
                  <p>Email: {details.email}</p>
                  <p>MobileNo: {details.mobileNo}</p>
              </ul>
            </Col>
          </Row>
          <Row className='pt-5 mt-5'>
              <Col>
                  <UpdateProfileForm />
              </Col>
          </Row>
        </Container>
      </>

	)
}