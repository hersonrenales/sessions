import {Form,Button} from 'react-bootstrap'
import {Row,Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Container from 'react-bootstrap/Container';
import UserContext from '../UserContext'
import {Navigate} from 'react-router-dom';
import adtwo from './images/ad2.mp4'

export default function Register(){

	const{user,setUser} = useContext(UserContext);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [image, setImage] = useState("");
	const[isActive, setIsActive] = useState(false);

	function registerUser(event){
		event.preventDefault();
		fetch('https://capstone2-renales.onrender.com/users/register',{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			
			if(data){
				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword("");
				setConfirmPassword("");
				setImage("");

				alert("Thank you for registering!");
			}else{
				alert("Please try again later.")
			}
		})
	}

	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword]
	)

	function convertToBase64(e){
		const reader = new FileReader();
		const file = e.target.files[0]
		reader.onload = () =>{
			const base64Image = reader.result
			console.log(base64Image);
			setImage(base64Image);
		}
		reader.onerror = error => {
			console.log("Error: ", error);
		}
		reader.readAsDataURL(file)
	}

	const clearImage = () => {
	    setImage("");
	    // Reset the file input
	    const inputElement = document.querySelector('input[type="file"]');
	    if (inputElement) {
	      inputElement.value = null;
	    }
	  };

	return(
		(user.id !== null)? 
    	<Navigate to="/products" />
    	:
    	<Container>
    		<Row>
    			<Col style={{margin:'30px'}}>
			    	<video width="320" height="auto" autoPlay loop muted>
                		<source src={adtwo} type="video/mp4" />
            		</video>
			    </Col>
    			<Col>
					<Form onSubmit={(event) => registerUser(event)}>
					<h3 className="my-3 text-center">Register</h3>
						<Form.Group>
							<Form.Label>First Name:</Form.Label>
							<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Last Name:</Form.Label>
							<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => {setLastName(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Email:</Form.Label>
							<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Mobile No:</Form.Label>
							<Form.Control type="number" placeholder="Enter 11 Digit No." required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Password:</Form.Label>
							<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Confirm Password:</Form.Label>
							<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}/>
						</Form.Group>
						<div className="auth-wrapper">
							<div className="auth-inner" style={{width: "auto"}}>
								Upload Image<br/>
								<input
									accept="image/*"
									type="file"
									onChange={convertToBase64}
								/>
								{image !== "" || image !== null}
									{image && (
									    <img width={100} height={100} src={image} alt="Product" />
									)}
							</div>
						</div>
						{
							isActive?
							<Button variant="primary" type="submit" className="mt-3">Submit</Button>
							:
							<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
						}
					</Form>
				</Col>
			</Row>
		</Container>
	)
}