import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import {Row,Col} from 'react-bootstrap';
import Swal from 'sweetalert2'
import adOne from './images/ad1.mp4'


export default function Login() {
	const{user,setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

	useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    },[email, password]);

	function authenticate(event) {
        event.preventDefault()
		fetch('https://capstone2-renales.onrender.com/users/login',{
		method: 'POST',
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			email: email,
			password: password
		})
	})
	.then(res => res.json())
	.then(data => {
		if(typeof data.access !== "undefined"){
			localStorage.setItem('token', data.access);
			retrieveUserDetails(data.access)
			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Happy Shopping"
			})
		} else {
			Swal.fire({
				title: "Authentication Failed",
				icon: "error",
				text: "Check your login details and try again!"
			})
		}
	})
	setEmail('');
	setPassword('');
    }

    const retrieveUserDetails = (token) => {
		fetch("https://capstone2-renales.onrender.com/users/details", {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

    return (
    	(user.id !== null)?
    	<Navigate to="/products" />
    	:
    	<Container>
    		<Row>
    			<Col style={{margin:'30px'}}>
			    	<video width="320" height="auto" autoPlay loop muted>
                		<source src={adOne} type="video/mp4" />
            		</video>
			    </Col>
    			<Col className="d-flex align-items-center">
				    <Form onSubmit={(e) => authenticate(e)}>
				    	<h3 className="my-3 text-center">Login</h3>
				        <Form.Group controlId="userEmail">
				            <Form.Label>Email address</Form.Label>
				            <Form.Control 
				                type="email" 
				                placeholder="Enter email" 
				                required
				                value={email}
				    			onChange={(e) => setEmail(e.target.value)}
				    			style={{ width: '350px' }}
				            />
				        </Form.Group>
				        <Form.Group controlId="password">
				            <Form.Label>Password</Form.Label>
				            <Form.Control 
				                type="password" 
				                placeholder="Password" 
				                required
				                value={password}
				    			onChange={(e) => setPassword(e.target.value)}
				    			style={{ width: '350px' }}
				            />
				        </Form.Group>

				        {
							isActive?
							<Button variant="primary" type="submit" className="mt-3">Submit</Button>
							:
							<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
						}
				    </Form>
			    </Col>
		    </Row>   
	    </Container>    
    )
}