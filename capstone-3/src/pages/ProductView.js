import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import adFive from './images/ad5.mp4';
import adSix from './images/ad6.mp4';


export default function ProducteView(){
	const {user} = useContext(UserContext);
	const {productId} = useParams();
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");

	const order = (productId) => {
		fetch(`https://capstone2-renales.onrender.com/users/order`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Successfully Ordered!",
					icon: "success",
					text: "You have successfully ordered this product."
				})

				navigate("/products");
			}else{
				Swal.fire({
					title: "Something Went Wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId);

		fetch(`https://capstone2-renales.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImage(data.image);
		})

	}, [productId])


	return(
		<Container className="mt-5">	
            <Row>
            	<Col lg={3}>
			    	<video width="320" height="auto" autoPlay loop muted>
                		<source src={adFive} type="video/mp4" />
            		</video>
			    </Col>
                <Col lg={6}>
                    <Card className="border-0">
                        <Card.Body className="text-center">
                        	<Card.Img src={image} style={{ maxWidth: "500px", maxHeight: "500px" }} alt={name} />
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            {
                            	user.id !== null ?
                            	<Button variant="primary" onClick={() => order(productId)}>Add to Cart</Button>
                            	:
                            	<Link className="btn btn-danger btn-block" to="/login">Login to Order</Link>
                            }    
                        </Card.Body>        
                    </Card>
                </Col>
                <Col lg={3}>
			    	<video width="320" height="auto" autoPlay loop muted>
                		<source src={adSix} type="video/mp4" />
            		</video>
			    </Col>
            </Row>
        </Container>
		)
}