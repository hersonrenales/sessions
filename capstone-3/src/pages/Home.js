import Banner from '../components/Banner';
import Highlights from '../components/Highlights'

export default function Home(){
	const data = {
        title: "Herson Online Shop",
        content: "New Products Everyday",
        destination: "/register",
        label: "Register"
    }
	return(
		<>
			<Highlights />
		</>
	)
}