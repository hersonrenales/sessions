import {Form,Button} from 'react-bootstrap'
import {Row,Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Container from 'react-bootstrap/Container';
import UserContext from '../UserContext'
import {Navigate} from 'react-router-dom';
import adtwo from './images/ad2.mp4'

export default function Register(){


	const{user,setUser} = useContext(UserContext);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [image, setImage] = useState("");
	const[isActive, setIsActive] = useState(false);

	function registerProduct(event){
		event.preventDefault();
		fetch('https://capstone2-renales.onrender.com/products/',{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
				setName("");
				setDescription("");
				setPrice("");
				setImage("");

				alert("Thank you for adding a product!");
			}else{
				alert("Please try again later.")
			}
		})
	}

	useEffect(() => {
		if((name !== "" && description !== "" && price !== "" && image !== "")){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	},[name,description,price,image]
	)

	function convertToBase64(e){
		const reader = new FileReader();
		const file = e.target.files[0]
		reader.onload = () =>{
			const base64Image = reader.result
			console.log(base64Image);
			setImage(base64Image);
		}
		reader.onerror = error => {
			console.log("Error: ", error);
		}
		reader.readAsDataURL(file)
	}

	const clearImage = () => {
	    setImage("");
	    // Reset the file input
	    const inputElement = document.querySelector('input[type="file"]');
	    if (inputElement) {
	      inputElement.value = null;
	    }
	  };

	return(
		 (user.isAdmin !== true) ? 
    	<Navigate to="/products" />
    	:
    	<Container>
    		<Row>
    			<Col style={{margin:'30px'}}>
			    	<video width="320" height="auto" autoPlay loop muted>
                		<source src={adtwo} type="video/mp4" />
            		</video>
			    </Col>
    			<Col>
					<Form onSubmit={(event) => registerProduct(event)}>
					<h3 className="my-3 text-center">Add Product</h3>
						<Form.Group>
							<Form.Label>Product Name:</Form.Label>
							<Form.Control type="text" placeholder="Enter Product Name" required value={name} onChange={e => {setName(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Product Image:</Form.Label>
							<div className="auth-wrapper">
								<div className="auth-inner" style={{width: "auto"}}>
									Upload Image<br/>
									<input
										accept="image/*"
										type="file"
										onChange={convertToBase64}
									/>
									{image !== "" || image !== null}
										{image && (
										    <img width={100} height={100} src={image} alt="Product" />
										)}
								</div>
							</div>
						</Form.Group>
						{
							isActive?
							<Button variant="primary" type="submit" className="mt-3" onClick={clearImage}>Submit</Button>
							:
							<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
						}
					</Form>
				</Col>
			</Row>
		</Container>
	)
}