import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
// import CourseSearch from './CourseSearch';


export default function UserView({productsData}) {
    const [products, setProducts] = useState([])

    useEffect(() => {
        const productsArr = productsData.map(product => {
            if(product.isActive === true) {
                return (
                    <ProductCard productProp={product} key={product._id}/>
                    )
            } else {
                return null;
            }
        })
        setProducts(productsArr)

    }, [productsData])

    return(
        <>
            {/*<CourseSearch />*/}
            {products}
        </>
        )
}