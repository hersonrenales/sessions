import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { Row, Col } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import { useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Image } from 'react-bootstrap';
import logoImage from './images/logo.png';
import searchButton from './images/searchbutton.png';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';

export default function AppHeader() {
  return (
    <Navbar className="header">
      <Container>
        <Row className="align-items-center">
          <Col xs="auto">
            <Navbar.Brand href="#home">
              <Image src={logoImage} style={{ width: 'auto', height: '100px' }} />
            </Navbar.Brand>
          </Col>
          <Col>
            <Form inline>
              <Row>
                <Col xs="auto" className="pr-0">
                  <Form.Control
                    type="text"
                    placeholder="Search"
                    className="mr-sm-2"
                    style={{width:'650px'}}
                  />
                  <p style={{color:'white',paddingTop:'0.5rem'}}>Free Shipping | Up to 50% OFF | HO Shop Pay Later | Buy 1 Take 1 | Fast Delivery | Trending</p>
                </Col>
                <Col xs="auto">
                  <Button type="submit" className="bg-transparent border-0 p-0 m-0">
                  	<Image src={searchButton} style={{ width: 'auto', height: '40px'}}/>
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    </Navbar>
  );
}

/*
export default function AppHeader() {
  return (
    <Navbar className="header">
      <Container className="p-0 m-0">
        <Row>
          <Navbar.Brand href="#home">
            <Image src={logoImage} style={{ width: 'auto', height: '100px' }} />
              <Form inline>
                <Row>
                  <Col xs="auto">
                    <Form.Control
                      type="text"
                      placeholder="Search"
                      className=" mr-sm-2"
                    />
                  </Col>
                  <Col xs="auto">
                    <Button type="submit">Submit</Button>
                  </Col>
                </Row>
              </Form>
          </Navbar.Brand>
        </Row>
      </Container>
    </Navbar>
  );
}
*/