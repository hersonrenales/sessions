import Container from 'react-bootstrap/Container';
import {Row,Col} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link,NavLink} from 'react-router-dom';
import{useState,useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar(){

	const {user} = useContext(UserContext);
	return(
		<Navbar className="navbar p-0 height-auto"style={{height: '1.5rem'}}>
			<Container>
				<Row>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
			      <Nav className="me-auto">
			        
			        {(user.isAdmin === true) ?
			        <Nav.Link as={NavLink} to="/addproduct" exact>| Add Product</Nav.Link>
			        :
			        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
			        }
			        <Nav.Link as={NavLink} to="/products" exact>| Products</Nav.Link>

			        {(user.id !== null)?
			        	<>
				            <Nav.Link as={NavLink} to="/profile" exact>| Profile</Nav.Link>
				            <Nav.Link as={NavLink} to="/logout" exact>| Logout</Nav.Link>
			            </>
			            :
			            <>
				            <Nav.Link as={NavLink} to="/login" exact>| Login</Nav.Link>
				            <Nav.Link as={NavLink} to="/register" exact>| Register</Nav.Link>
			            </>
			        }

			        
			      </Nav>
			    </Navbar.Collapse>
			    </Row>
			</Container>
	    </Navbar>
	)
}