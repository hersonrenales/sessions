import {Card,Button} from 'react-bootstrap';
import {Row,Col} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {useState} from 'react';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){
	const {_id,name,description,price,image}=productProp;
	const [count, setCount]=useState(100)
	
	return (
	  <Col xs={12} md={4} className="justify-content-end "  style={{display: 'inline-flex', maxWidth: "300px", maxHeight: "300px", marginBottom: '100px', marginTop: '50px'   }}>
	        <Card  style={{ border: 'none' }}>
	          <Card.Img src={image} style={{ maxWidth: "200px", maxHeight: "200px" }} alt={name} />
	          <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}.</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>Php {price}</Card.Text>
	            <Card.Text>Stocks: {count}</Card.Text>
	            <Link className="btn btn-primary" to={`/products/${_id}`}>Buy</Link>
	          </Card.Body>
	        </Card>
	      </Col>
	    );
	  }

ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		productImage: PropTypes.string.isRequired
	})
}
