import {Card,Row,Col,Button} from 'react-bootstrap'
import PropTypes from 'prop-types';
import {useContext, useState, useEffect} from 'react'
import adthree from './images/ad3.mp4'
import adfour from './images/ad4.mp4'

const cardStyle = {
  border: 'none', 
  textAlign: 'center'
};

const videoStyle = {
  maxWidth: '1000px',
  maxHeight: '1000px',
};


export default function Highlights(){
	const [details,setDetails] = useState({});
	useEffect(() => {
    fetch(`https://capstone2-renales.onrender.com/products/all`, {})
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setDetails(data);
      });
  	}, []); 
	
	return(
	<>
		<Row>
		      <Col className="justify-content-center d-flex align-items-center mt-5">
		        <video style={{maxWidth: "800px", maxHeight:"800px"}} autoPlay loop muted>
		          <source src={adthree} type="video/mp4" />
		        </video>
		      </Col>
		      <Col className="justify-content-center d-flex align-items-center mt-5">
		        <video style={{maxWidth: "500px", maxHeight:"500px"}} autoPlay loop>
		          <source src={adfour} type="video/mp4" />
		        </video>
		      </Col>
		    </Row>
		<Row>
			{details.length > 0 && details[7] && (
	          <Col>
	            <Card style={cardStyle} className="cardHighlight p-3">
	              <Card.Body>
	                <Card.Title>
	                  <h2>{details[7].name}</h2>
	                </Card.Title>
	                <img
	                  src={details[7].image}
	                  style={{ maxWidth: "200px", maxHeight: "200px" }}
	                  alt={details[7].name}
	                />
	                <Card.Text>{details[7].description}</Card.Text>
	                <Button variant="primary">Buy</Button>
	              </Card.Body>
	            </Card>
	          </Col>
	        )}
	        {details.length > 0 && details[15] && (
			<Col>
				<Card style={cardStyle} className="cardHighlight p-3">
					<Card.Body>
					<Card.Title>
						<h2>{details[15].name}</h2>
					</Card.Title>
					<img src={details[15].image} style={{ maxWidth: "200px", maxHeight: "200px" }}/>
					<Card.Text>{details[15].description}</Card.Text>
					<Button variant="primary" >Buy</Button>
					</Card.Body>
			    </Card>
			</Col>
			)}
			{details.length > 0 && details[23] && (
			<Col>
				<Card style={cardStyle} className="cardHighlight p-3">
					<Card.Body>
					<Card.Title>
						<h2>{details[23].name}</h2>
					</Card.Title>
					<img src={details[23].image} style={{ maxWidth: "200px", maxHeight: "200px" }}/>
					<Card.Text>{details[23].description}</Card.Text>
					<Button variant="primary" >Buy</Button>
					</Card.Body>
			    </Card>	
			</Col>
			)}
		</Row>
		<Row>
			{details.length > 0 && details[8] && (
			<Col>
				<Card style={cardStyle} className="cardHighlight p-3">
					<Card.Body>
					<Card.Title>
						<h2>{details[8].name}</h2>
					</Card.Title>
					<img src={details[8].image} style={{ maxWidth: "200px", maxHeight: "200px" }}/>
					<Card.Text>{details[8].description}</Card.Text>
					<Button variant="primary" >Buy</Button>
					</Card.Body>
			    </Card>
			</Col>
			)}
			{details.length > 0 && details[12] && (
			<Col>
				<Card style={cardStyle} className="cardHighlight p-3">
					<Card.Body>
					<Card.Title>
						<h2>{details[12].name}</h2>
					</Card.Title>
					<img src={details[12].image} style={{ maxWidth: "200px", maxHeight: "200px" }}/>
					<Card.Text>{details[12].description}</Card.Text>
					<Button variant="primary" >Buy</Button>
					</Card.Body>
			    </Card>
			</Col>
			)}
			{details.length > 0 && details[10] && (
			<Col>
				<Card style={cardStyle} className="cardHighlight p-3">
					<Card.Body>
					<Card.Title>
						<h2>{details[10].name}</h2>
					</Card.Title>
					<img src={details[10].image} style={{ maxWidth: "200px", maxHeight: "200px" }}/>
					<Card.Text>{details[10].description}</Card.Text>
					<Button variant="primary" >Buy</Button>
					</Card.Body>
			    </Card>
			</Col>
			)}
		</Row>
	</>
	)
}