import AppNavbar from './components/AppNavbar';
import AppHeader from './components/AppHeader';
import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Profile from './pages/Profile'
import Error from './pages/Error';
import ProductView from './pages/ProductView';
import RegisterProduct from './pages/RegisterProduct';
import {UserProvider} from './UserContext';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Routes} from 'react-router-dom'
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import './App.css';

function App() {
  const[user,setUser] = useState({
    id: null,
    isAdmin: null
  });
  const unsetUser = () => {
    localStorage.clear();
  }
  
    useEffect(() => {
    fetch(`https://capstone2-renales.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });

      }
    })
    }, []);
    
  return (
    <UserProvider value = {{user,setUser,unsetUser}}>
      <Router>
          <Container fluid>
            <AppNavbar />
            <AppHeader />
            <Routes>
              <Route path="/" element = {<Home />} />
              <Route path="/products" element = {<Products />} />
              <Route path="/products/:productId" element={<ProductView />}/>
              <Route path="/register" element = {<Register />} />
              <Route path="/login" element = {<Login />} />
              <Route path="/logout" element = {<Logout />} />
              <Route path="/profile" element = {<Profile />} />
              <Route path="/addproduct" element = {<RegisterProduct />} />
              <Route path="*" element = {<Error />} />
            </ Routes>
          </Container>
        </Router>
      </UserProvider>
    );
}

export default App;

/*
 return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <Routes>
            <Route
              path="/"
              element={
                <>
                  <AppNavbar />
                  <AppHeader />
                  <Home />
                </>
              }
            />
            <Route
              path="/products"
              element={
                <>
                  <AppNavbar />
                  <AppHeader />
                  <Products />
                </>
              }
            />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
*/