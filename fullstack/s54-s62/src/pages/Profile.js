import {useContext} from 'react';
import {Row,Col} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Profile() {
    const {user} = useContext(UserContext);

    return(
      (user.access === null)?
        <Navigate to="/courses"/>
      :
      <Row className="profile px-5">
        <Col>
          <h1 className="mt-5 pt-5">Profile</h1>
          <h2 className="mt-5">James Dela Cruz</h2>
          <hr />
          <h4>Contacts</h4>
          <ul className="px-5">
              <li>Email: {user.email}</li>
              <li>MobileNo: {user.mobileNo}</li>
          </ul>
        </Col>
      </Row>
    )
}
