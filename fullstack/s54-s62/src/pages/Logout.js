import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext'

export default function Logout() {

    const {unsetUser, setUser} = useContext(UserContext)
    // updates localStorage to empty or  // localStorage.clear();
    unsetUser(); 
    // localStorage.clear();

    useEffect(() => {
        // setter function - reverts the user state saved in context back to null
        setUser({
            id: null,
            isAdmin: null
        });
    })

    // Redirect back to login
    return (
        <Navigate to='/login' />
    )

}