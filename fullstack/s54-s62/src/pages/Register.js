import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

export default function Register(){

	const{user,setUser} = useContext(UserContext);

	// State hooks to store values of the input fields
	/*
	Syntax:
	[stateVariable, setterFunction] = useState(assigned state/initial state)
	*/
	// Note: setterFunction is to update the state value
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const[isActive, setIsActive] = useState(false);

	// Checks if values are successfully binded 
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password);
	console.log(confirmPassword);

	function registerUser(event){
		// Prevents the default behaviour during submission which is page redirection via form submission
		event.preventDefault();
		fetch('http://localhost:4000/users/register',{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				access: localStorage.getItem('token')
			});
			// data is the response from the web service
			console.log(data);

			// if the received response is true states will be emptied and the user will receive an alert message, else try again.
			if(data){
				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword("");
				setConfirmPassword("");

				alert("Thank you for registering!");
			}else{
				alert("Please try again later.")
			}
		})
	}

	// The useEffect will run everytime the state changes in any of the field or values listed in [firstName,lastName,email,mobileNo,password,confirmPassword]
	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			// sets the isActive value to true to enable the button
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword]
	)

	return(
		(user.access !== null)? 
    	<Navigate to="/courses" />
    	:
		<Form onSubmit={(event) => registerUser(event)}> 
		<h1 className="my-5 text-center">Register</h1>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				{/* Two-way binding*/}
				<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => {setLastName(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile No:</Form.Label>
				<Form.Control type="number" placeholder="Enter 11 Digit No." required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}/>
			</Form.Group>
			{/* Conditional Rendering:
				- if isActive is true - enabled button. else, disabled button
			 */}
			{
				isActive?
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
			}
			
		</Form>
	)
}