const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dictum, metus commodo euismod maximus, odio metus suscipit ante, dapibus vestibulum nulla sem ut mi. Integer viverra erat eros. Nam elit nisl, tempor quis odio eget, hendrerit euismod risus. Etiam dictum luctus venenatis. Pellentesque tempus, eros vel viverra interdum, mauris urna efficitur odio, vitae bibendum erat mi in lacus. Curabitur sit amet tellus nec enim porttitor consequat. Curabitur dignissim non sapien eu feugiat.",
		price: "45000",
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Nunc vitae ultrices diam, eget ultricies augue. Proin imperdiet, nibh eu finibus blandit, nisi lacus vulputate velit, hendrerit congue enim odio eu velit. Nulla quis ligula nec odio dapibus vulputate. Morbi congue est sed diam interdum, at tempus est condimentum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis vestibulum risus et lacus fermentum laoreet. Donec quis dolor dignissim, convallis purus id, sagittis tellus.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Donec ac nibh orci. Donec sed dolor ipsum. Sed id feugiat ligula, in gravida nulla. Fusce imperdiet nisi eu sapien gravida elementum. Duis ac justo vitae turpis dictum auctor. Morbi ultricies libero nec aliquet semper. Phasellus tristique tempus ipsum, eget vehicula metus. Sed at luctus enim. Suspendisse potenti. Suspendisse metus nibh, tristique ac massa ut, auctor tempor ipsum.",
		price: 55000,
		onOffer: true
	}
]

export default coursesData;