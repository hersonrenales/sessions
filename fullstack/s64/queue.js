let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(item){
    // add an item
    collection.push(item);
    return collection;

}

function dequeue(){
    // remove an item
    collection.shift();
    return collection;
}

function front(){
    // get the front item
    const frontItem = collection[0];
    return frontItem
}

function size(){
    // get the size of the array
    const arrSize = collection.length;
    return arrSize

}

function isEmpty(){
    // check array if empty
  return collection.length === 0;

}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};