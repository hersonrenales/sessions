// [ SECTION ] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');

const userRoutes = require('./routes/user'); // Allows access to routes defined within our application

const productRoutes = require("./routes/product.js")

// [ SECTION ] Environment Setup
const port = 4000;

// [ SECTION ] Server Setup
const app = express();

app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Allows all resources to acces our backend application;
app.use(cors()); // CORS is a security feature implemented by web browsers to control requests to a different domain than the one the website came from.

// [ SECTION ] Database Connection
mongoose.connect('mongodb+srv://admin:admin123@cluster0.4skkann.mongodb.net/Capstone-3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// [ SECTION ] Backend Routes
app.use('/users', userRoutes);

// back end routes for product
app.use('/products', productRoutes);

// [ SECTION ] Server Gateway Response
if(require.main === module){
	app.listen(port, () => {
		console.log(`API is now online on port ${port}`);
	});
};

module.exports = app;