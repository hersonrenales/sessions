const Product = require("../models/Product.js");

module.exports.addProduct = (req, res) => {

	let newProduct = new Product({
		name : req.body.name,
		description: req.body.description,
		price: req.body.price,
		image: req.body.image
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
}

module.exports.getAllProducts = (req, res) => {
	return Product.find({}).then(result => {
		if(result.length === 0){
			return res.send("There is no products in the DB.");
		}else{
			return res.send(result);
		}
	})
}

module.exports.getAllActive = (req, res) => {
	return Product.find({isActive : true}).then(result => {
		if(result.length === 0){
			return res.send("There is currently no active products.")
		}else{
			return res.send(result);
		}
	})
}

module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result =>{
		if(result === 0){
			return res.send("Cannot find product with the provided ID.")
		}else{
			if(result.isActive === false){
				return res.send("The product you are trying to access is not available.");
			}else{
				return res.send(result);
			}
		}
	})
	.catch(error => res.send("You provided a wrong product ID"));
}


module.exports.updateProduct = (req, res) => {
	let updatedProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
}

module.exports.archiveProduct = (req, res) => {
	let updateActiveField = {
		isActive: false
	}

	return Product.findByIdAndUpdate(req.params.productId, updateActiveField).then((product, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
}

module.exports.activateProduct = (req, res) => {
	let updateActiveField = {
		isActive: true
	}

	return Product.findByIdAndUpdate(req.params.productId, updateActiveField).then((product, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
}

module.exports.archives = (req, res) => {
	return Product.find({isActive: false}).then(result => {
		if(result.length === 0){
			return res.send("There is no products in the archives.");
		}else{
			return res.send(result);
		}
	})
	.catch(error => res.send("zzz"));
}
