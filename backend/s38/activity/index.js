let http = require("http");

let port = 4000;

let app = http.createServer(function(request, response){
	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to Booking System");
	}
	
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to your profile");
	}

	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Here's our courses available");
	}

	if(request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Add course to our resources");
	}

	if(request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Update a course to our resources");
	}

	if(request.url == "/archivecourse" && request.method == "DELETE"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Archive a course to our resources");
	}

})

app.listen(port, () => console.log('Server is running at localhost:4000'));