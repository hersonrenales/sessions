const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js");

// Server Setup + Middleware
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// default endpoint
// localhost:4000/tasks/
app.use("/tasks", taskRoute);

// DB Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.4skkann.mongodb.net/B305-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Notification for connection status
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error!"));
db.once("open", () => console.log("We're connected to the cloud database."));


// Server Listening
if(require.main === module){
	app.listen(port,() => console.log(`Server is running at port ${port}`))
}