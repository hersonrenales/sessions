// While Loop
/*
Syntax:
while(condition/expression){
	code block
}
*/
 let count = 5;

 while(count !== 0){
 	console.log("While: " + count);
 	count--;
 }

 // Do While Loop
 /*
do{
	code
}while(expression/condition)
 */

 let number = Number(prompt("Give me a number."));

 do{
 	console.log("Do While: " + number);
 	number++;
 }while(number <= 10);

// For Loop
/*
for(initialization; condition; iterator){
	code
}
*/

 for(let count = 0; count <= 20; count++){
 	console.log(count)
 }

 let myString = "alex";
 console.log(myString.length);
 

 for(let x = 0; x < myString.length; x++){
 	console.log(myString[x]);
 }

let myName = "Alex";

for(let i = 0; i < myName.length; i++){
	// console.log(myName[i].toLowerCase());
	if(myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"){
		console.log(3);
	}else{
		console.log(myName[i])
	}
}

for(let count = 0; count <= 100; count++){
	if(count % 2 === 0){
		continue;
	}/*else if(count > 50){
		break;
	}*/
	console.log("Continue and Break: " + count)
	if(count > 5){
		break;
	}
}

let name = "alexandro";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);
	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	if(name[i] == "d"){
		break;
	}
}