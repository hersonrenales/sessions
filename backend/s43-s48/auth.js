// [SECTION] Dependencies and Modules
const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
};

// Verify Token
module.exports.verify = (req,res,next) => {
	// Token is retrieved from the request header
	// Authorization (Auth Tab) -> Bearer Token

	console.log(req.headers.authorization);
	let token = req.headers.authorization;
	if(typeof token === "undefined"){
		return res.send({auth: "Failed! No Token"})
	}else{
		console.log(token)
		/*
		Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0ZmIxNGY2MTIyNjZhNWUxYTBmNWI5MiIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2OTQ0Mjk3NDZ9.5H-Fv-22XhSwvBuJZJxq46pCkxfNSCUPwzNfv8jx0tk
		*/
		token = token.slice(7, token.length);
		console.log(token);
		jwt.verify(token,secret, function(error, decodedToken){
			if(error){
				return res.send({
					auth: "Failed",
					message: error.message
				});
			}else{
				console.log(decodedToken);
				// Contains data from our token
				req.user = decodedToken;
				next();
				// Will let us proceed to the next controller
			}
		})
	}
}

// Verify if user account is admin or not
module.exports.verifyAdmin = (req,res,next) => {
	if(req.user.isAdmin){
		next()
	}else{
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}