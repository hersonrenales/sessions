// [SECTION] Dependencies and Modules
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth.js")

// Destructure from auth
const {verify, verifyAdmin} = auth

// [SECTION] Routing Component
const router = express.Router();

// Check Email Routes
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
})

// User Registration Routes
router.post("/register", (req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Authentication
router.post("/login", userController.loginUser)

// Get User Info
/*
router.post("/details", verify, (req,res) =>{
	userController.getProfile(req.user.id).then(resultFromController => res.send(resultFromController));
});
*/
router.get("/details", verify, userController.getProfile);

// Enroll User to a Course
router.post("/enroll", verify, userController.enroll);

// Get User Enrolled Courses
router.get("/getenrollments", verify , userController.getEnrollments);

// Reset the Password
router.put("/reset-password", verify, userController.resetPassword);

// [SECTION] Export Route System
module.exports = router;