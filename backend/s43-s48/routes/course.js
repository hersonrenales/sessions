// [SECTION] Dependencies and Modules
const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth.js")

// Destructure from auth
const {verify, verifyAdmin} = auth

// [SECTION] Routing Component
const router = express.Router();

// Course Registration Routes
router.post("/", verify, verifyAdmin, courseController.addCourse); 

// Get All Courses
router.get("/all", courseController.getAllCourses);

// Get All "active" course
router.get("/", courseController.getAllActive);

// Get 1 Specific Course
router.get("/:courseId", courseController.getCourse);

// Updating a Course (Admin Only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// Archiving a Course
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

// Activating a Course
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

// [SECTION] Export Route System
module.exports = router;