// [SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");
	require('dotenv').config();
	// Allows access to routes defined within our application
	const userRoutes = require("./routes/user"); 
	const courseRoutes = require("./routes/course"); 
// [SECTION] Environment Setup
	const port = 4000;

// [SECTION] Server Setup
	const app = express();

	app.use(express.json());
	app.use(express.urlencoded({extended:true}));
	app.use(cors()); // allows all resources to access our backend application

// [SECTION] Database Connection
	mongoose.connect("mongodb+srv://admin:admin123@cluster0.4skkann.mongodb.net/S43-S48?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

	mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// [SECTION] Backend Routes
	app.use("/users", userRoutes);
	app.use("/courses", courseRoutes);

// [SECTION] Server Gateway Response
	if(require.main === module){
		app.listen(process.env.PORT || port, () => {
			console.log(`API is now online on port ${process.env.PORT || port}`)
		})
	}

module.exports = app;
