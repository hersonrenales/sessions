// Node.js
// Client-server architecture

// Client applications -> browsers
// DB Servers -> systems developed using node
// DB - MongoDB

// Benefits
// 1. Performance -> optimized for web applications
// 2. Familiarity - Same old JS
// 3. NPM - Node Package Manager

// Use the "require" directive to load Node.js
// "http module" contains the components needed to create a server.
let http = require("http");

// "createServer()" listens to a request from a client.
// A port is a vitrual point where network connection start and end.
// ".listen()" where the server will listen to any request sent in our server.
http.createServer(function(request, response){

	// "writeHead()" sets the status code for response
	// status code 200 means OK
	response.writeHead(200, {"Content-Type": "text/plain"});
	// Send the response with text content "Hello World!"
	response.end("Hello World!");

}).listen(4000);

// Output will be shown at the terminal
console.log("Server is running at localhost:4000");